import { deleteMoney, checkRemoveAddClass, noMoney, getRandom, addMoney, getRandom_2 } from './functions.js';

if (sessionStorage.getItem('money')) {
	if (document.querySelector('.check')) {
		document.querySelectorAll('.check').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
} else {
	sessionStorage.setItem('money', 10000);
	if (document.querySelector('.check')) {
		document.querySelectorAll('.check').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
}

if (document.querySelector('.game')) {
	if (+sessionStorage.getItem('money') >= 100) {
		sessionStorage.setItem('current-bet', 100);
		document.querySelector('.block-bet__coins').textContent = sessionStorage.getItem('current-bet');
	} else {
		sessionStorage.setItem('current-bet', 0);
		document.querySelector('.block-bet__coins').textContent = 0;
	}
}

const window_width = document.documentElement.clientWidth;
const window_height = document.documentElement.clientHeight;

//========================================================================================================================================================
//main

export const prices = {
	price_2: 12500,
	price_3: 16000,
}
const coeffs = {
	coeff_first_airplane: 1.2,
	coeff_second_airplane: 1.5,
	coeff_third_airplane: 2.1,
}
if (document.querySelector('.main')) {
	document.querySelector('.main').classList.add('_active');
	drawPrices();
	drawStartCurrentAirplane();
	checkBoughtAirplanes();
	checkCurrentAirplane();
	drawCurrentAirplaneMainScreen();
}
function drawPrices() {
	document.querySelector('[data-price="2"]').textContent = prices.price_2;
	document.querySelector('[data-price="3"]').textContent = prices.price_3;
}
export function checkBoughtAirplanes() {
	if (sessionStorage.getItem('airplane-2')) {
		document.querySelector('[data-shop-button="2"] p').textContent = 'select';
		document.querySelector('[data-shop-button="2"]').classList.add('_bought');
		document.querySelector('[data-airplane="2"]').classList.add('_bought');
	}
	if (sessionStorage.getItem('airplane-3')) {
		document.querySelector('[data-shop-button="3"] p').textContent = 'select';
		document.querySelector('[data-shop-button="3"]').classList.add('_bought');
		document.querySelector('[data-airplane="3"]').classList.add('_bought');
	}
}
function checkCurrentAirplane() {
	let airplane = +sessionStorage.getItem('current-airplane');
	checkRemoveAddClass('.shop__item', '_selected', document.querySelectorAll('.shop__item')[airplane - 1]);
}
function drawStartCurrentAirplane() {
	if (!sessionStorage.getItem('current-airplane')) sessionStorage.setItem('current-airplane', 1);
}
export function drawCurrentAirplaneMainScreen() {
	const item = document.querySelector('.main__airplane');
	if (item.querySelector('img')) item.querySelector('img').remove();

	const image = document.createElement('img');
	const currentAir = sessionStorage.getItem('current-airplane');
	image.setAttribute('src', `img/game/airplane-${currentAir}.png`);
	item.append(image);
}
//========================================================================================================================================================
//game
export const config_game = {
	state: 1, // (1 - готов начать игру, 2 - летим, 3 - проиграли)
	height_field: document.querySelector('.game') ? document.querySelector('.field__body').clientHeight : false,

	current_win: 0,

	current_coeff: 0,
	airplane_coeff: 1.2,
	start_coeff: 0,
	coeff_up: 0.01,

	speed_up: 0.2,
	speed_right: 0.4,

	timerId: false,
	timerCoeff: false,
	timerDraw: false,
}
const config_canvas = {
	width: 0,
	height: 0,
	color_line: 0,
}
if (document.querySelector('.game')) {
	drawCurrentAirplane();
	drawMeasureSvgCircle();
	setCanvas();
	drawAirplaneCoeff();
	drawCurrentBg();
}
function drawCurrentBg() {
	const airplane = +sessionStorage.getItem('current-airplane');
	const fieldBody = document.querySelector('.field__body');
	if (airplane === 1) {
		fieldBody.classList.add('_bg-1');
	} else if (airplane === 2) {
		fieldBody.classList.add('_bg-2');
	} else if (airplane === 3) {
		fieldBody.classList.add('_bg-3');
	}
}

// draw start elements
function drawCurrentAirplane() {
	const image = document.createElement('img');
	image.setAttribute('src', `img/game/airplane-${sessionStorage.getItem('current-airplane')}.png`);
	document.querySelector('.field__airplane').append(image);
}
function drawMeasureSvgCircle() {
	let svg = document.querySelector('.field__circle circle');
	let svg_m = document.querySelector('.field__circle svg');

	svg.setAttribute('r', config_game.height_field * 4);
	svg.setAttribute('stroke-width', config_game.height_field * 8);

	svg_m.setAttribute('width', config_game.height_field * 8);
	svg_m.setAttribute('height', config_game.height_field * 8);

	document.querySelector('.field__circle').style.left = `-${config_game.height_field * 4}px`;
	document.querySelector('.field__circle').style.bottom = `${config_game.height_field * 3}px`;
	if (window_height >= 600) svg.setAttribute('stroke-dasharray', 300);
}
function addRotateCircleBg() {
	document.querySelector('.field__circle svg').classList.add('_rotate');
}
function removeRotateCircleBg() {
	if (document.querySelector('.field__circle svg').classList.contains('_rotate')) {
		document.querySelector('.field__circle svg').classList.remove('_rotate');
	}
}

function drawAirplaneCoeff() {
	const coeff = +sessionStorage.getItem('current-airplane');
	if (coeff === 2) {
		config_game.airplane_coeff = coeffs.coeff_second_airplane;
	} else if (coeff === 3) {
		config_game.airplane_coeff = coeffs.coeff_third_airplane
	}
}

// game logic
export function startGame() {
	config_game.state = 2;
	generateCoeff();

	deleteMoney(+sessionStorage.getItem('current-bet'), '.check');

	document.querySelector('.footer__bet-box').classList.add('_hold');
	if (document.querySelector('.footer__button-cash').classList.contains('_hold'))
		document.querySelector('.footer__button-cash').classList.remove('_hold');

	addRotateCircleBg();
	generateStartSpeed();
	generateLineColor();
	moovePlayer();
	intervalCoeff();
}
function generateCoeff() {
	let state = getRandom(1, 10);
	if (state > 0 && state <= 4) {
		config_game.current_coeff = getRandom_2(1.1, 3);
	} else if (state > 4 && state <= 8) {
		config_game.current_coeff = getRandom_2(3, 5);
	} else if (state > 8) {
		config_game.current_coeff = getRandom_2(5, 10);
	}
}

function generateStartSpeed() {
	config_game.speed_up = getRandom_2(0.11, 0.25);
	config_game.speed_right = getRandom_2(0.4, 0.7);
}

function generateLineColor() {
	config_canvas.color_line = `${getRandom(0, 255)},${getRandom(0, 255)},${getRandom(0, 255)}`;
}

function moovePlayer() {
	let bottom = 7;
	let left = 2;
	let rotate = -35;
	let player = document.querySelector('.field__airplane');
	drawCanvas();
	config_game.timerId = setInterval(() => {
		if (left <= 15) {
			bottom += config_game.speed_up;
		} else if (left > 15 && left <= 30) {
			bottom += config_game.speed_up * 1.5;
		} else if (left > 30 && left <= 45) {
			bottom += config_game.speed_up * 2;
		} else if (left > 45) {
			bottom += config_game.speed_up * 4;
		}
		left += config_game.speed_right;

		if (rotate >= -50) {
			rotate -= 0.2;
		}

		player.style.bottom = `${bottom}%`;
		player.style.left = `${left}%`;
		player.style.transform = `rotate(${rotate}deg)`;

		if (left >= 70 || bottom >= 70) {
			clearInterval(config_game.timerId);
			player.style.transition = `all 1s ease 0s`;
			player.style.transform = `rotate(-40deg)`;
			document.querySelector('.field__body').classList.add('_fly');
		}
	}, 35);
}

function checkGameOver() {
	if (config_game.start_coeff >= config_game.current_coeff) {
		flyAirplaneWhenLoose();
		config_game.state = 3;
		stopAnimation();
		addLooseColorButtons();

		document.querySelector('.footer__button-cash').classList.add('_hold');

		setTimeout(() => {
			resetGame();
		}, 2000);
	}
}

function addLooseColorButtons() {
	document.querySelector('.footer__body').classList.add('_loose');
}

function flyAirplaneWhenLoose() {
	let player = document.querySelector('.field__airplane');
	player.style.transition = `all 2s ease 0s`;
	player.style.left = `150%`;
	player.style.bottom = `110%`;
}

//========
//canvas
function setCanvas() {
	let canvas = document.querySelector('.ctx');

	let block = document.querySelector('.field__body');
	config_canvas.width = block.clientWidth;
	config_canvas.height = block.clientHeight;

	canvas.setAttribute('width', `${config_canvas.width}px`);
	canvas.setAttribute('height', `${config_canvas.height}px`);
}
function createLineCanvas(x, y, color, lineWidth, rx, ry) {
	let canvas = document.querySelector('.ctx');
	let ctx = canvas.getContext('2d');

	ctx.lineWidth = lineWidth;

	let gradient = ctx.createLinearGradient(0, config_canvas.height, x, y);

	gradient.addColorStop(0, `rgba(${color},0.1)`);
	gradient.addColorStop(1, `rgba(${color},1)`);
	ctx.strokeStyle = gradient;

	ctx.beginPath();
	ctx.moveTo(0, config_canvas.height);
	ctx.quadraticCurveTo(x - rx, y + ry, x, y);

	ctx.stroke();
}

function createShapeCanvas(xTop, yTop, color, rx, ry) {
	let canvas = document.querySelector('.ctx');
	let ctx = canvas.getContext('2d');

	const gradient = ctx.createLinearGradient(xTop, yTop, 0, config_canvas.height);

	gradient.addColorStop(0, `rgba(${color},0.6)`);
	gradient.addColorStop(1, `rgba(${color},0.1)`);

	ctx.fillStyle = gradient;


	ctx.beginPath();

	// устанавливаем первую точку фигуры
	ctx.moveTo(0, config_canvas.height);

	// вторая точка - кривая линия, повторяющая основную линию

	ctx.quadraticCurveTo(xTop - rx, yTop + ry, xTop, yTop);

	// третья точка - динамическая
	ctx.lineTo(xTop, config_canvas.height);

	ctx.closePath();

	ctx.fill();
}

function clearCanvas() {
	let canvas = document.querySelector('.ctx');
	let ctx = canvas.getContext('2d');
	ctx.clearRect(0, 0, config_canvas.width, config_canvas.height);
}
function drawCanvas() {

	let pin = document.querySelector('.field__pin');
	let canvas = document.querySelector('.ctx');
	let ctx = canvas.getContext('2d');

	config_game.timerDraw = setInterval(() => {
		if (config_game.state == 3) {
			setTimeout(() => {
				clearInterval(config_game.timerDraw);
			}, 800);
		}
		let coord_x = pin.getBoundingClientRect().left - 70;
		let coord_y = pin.getBoundingClientRect().top - 70;

		ctx.clearRect(0, 0, config_canvas.width, config_canvas.height);
		createShapeCanvas(coord_x, coord_y, config_canvas.color_line, 10, 15);
		createLineCanvas(coord_x, coord_y, config_canvas.color_line, '4', 10, 15);
	}, 50);
}
//======
function intervalCoeff() {
	config_game.timerCoeff = setInterval(() => {
		config_game.start_coeff += config_game.coeff_up;
		drawCurrentCoeff();
		drawCurrentCount();
		checkGameOver();
	}, 20);
}

function drawCurrentCount() {
	let bet = +sessionStorage.getItem('current-bet');
	config_game.current_win = Math.floor(bet * config_game.start_coeff * config_game.airplane_coeff);
	document.querySelector('.footer__current-count').textContent = config_game.current_win;
}

function drawCurrentCoeff() {
	document.querySelector('.field__coeff').textContent = `${config_game.start_coeff.toFixed(2)}x`;
}

export function stopAnimation() {
	clearInterval(config_game.timerId);
	clearInterval(config_game.timerCoeff);
	if (document.querySelector('.field__body').classList.contains('_fly'))
		document.querySelector('.field__body').classList.remove('_fly');
}

function removeGameColorButtons() {
	if (document.querySelector('.footer__body').classList.contains('_loose'))
		document.querySelector('.footer__body').classList.remove('_loose');
}

export function resetGame() {
	document.querySelector('.field__body').classList.add('_loader');

	let player = document.querySelector('.field__airplane');

	config_game.current_win = 0;
	config_game.start_coeff = 0;
	config_game.state = 1;
	player.style.transition = `none`;
	player.style.left = `2%`;
	player.style.bottom = `7%`;
	player.style.transform = `rotate(-35deg)`;

	document.querySelector('.footer__current-count').textContent = 0;
	document.querySelector('.field__coeff').textContent = '0x';

	removeGameColorButtons();
	removeRotateCircleBg();
	clearCanvas();

	if (document.querySelector('.field__body').classList.contains('_fly'))
		document.querySelector('.field__body').classList.remove('_fly');

	setTimeout(() => {
		if (document.querySelector('.footer__bet-box').classList.contains('_hold'))
			document.querySelector('.footer__bet-box').classList.remove('_hold');

		checkRemoveAddClass('.footer__button', '_active', document.querySelector('[data-footer-button="bet"]'));
		document.querySelector('.field__body').classList.remove('_loader');
	}, 5000);
}

export function autoMode() {
	let bet = getRandomBet();
	setTimeout(() => {
		sessionStorage.setItem('current-bet', bet);
		document.querySelector('.block-bet__coins').textContent = sessionStorage.getItem('current-bet');
	}, 500);
	setTimeout(() => {
		startGame();
	}, 1500);
}

function getRandomBet() {
	let money = +sessionStorage.getItem('money');
	let random_bet = getRandom(5, money);
	if (random_bet % 5 != 0) {
		random_bet = random_bet - random_bet % 5;
	}
	return random_bet;
}

